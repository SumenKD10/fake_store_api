const logoutButton = document.querySelector("#Logout");
const signUp = document.querySelector("#signUp");

//Getting User Details
function checkUser() {
  const navDiv = document.querySelector(".navButtons");
  const userDiv = document.querySelector(".userLoggedIn");
  const userGot = localStorage.getItem("user");
  parsedUserGot = JSON.parse(userGot);
  console.log(parsedUserGot);
  if (userGot === null) {
    userDiv.style.display = "none";
    navDiv.style.display = "block";
  } else {
    console.log("Cat");
    let nameFill = document.querySelector("#yourName");
    let mailFill = document.querySelector("#yourMail");
    nameFill.innerText = parsedUserGot.name;
    mailFill.innerText = parsedUserGot.mail;
    navDiv.style.display = "none";
    userDiv.style.display = "block";
  }
}

function allProducts() {
  const url = "https://fakestoreapi.com/products";
  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if (data === null) {
        throw new Error("No Data Found");
      } else if (Array.isArray(data) === false) {
        throw new Error("Invalid Data");
      } else {
        removeLoader();
        addData(data);
      }
    })
    .catch((errorMessage) => {
      removeLoader();
      const error = document.createElement("p");
      error.setAttribute("class", "errorMessage");
      error.innerText =
        "Internal Server Error! \n\n The Server Entcountered an Internal Server Error Misconfiguration and was unable to complete your request.";
      console.error(errorMessage);
      contentSection.appendChild(error);
      contentSection.style.height = "calc(91vh - " + error.offsetHeight + "px)";
    });
}

function removeLoader() {
  let loaderElement = document.querySelector(".lds-ring");
  loaderElement.remove();
}

function addData(allProductData) {
  const contentSection = document.querySelector("#contentSection");
  let allData = allProductData.map((eachData) => {
    let creatingOneDiv = document.createElement("div");
    creatingOneDiv.setAttribute("class", "product");

    //Make an Image container
    let imageContainer = document.createElement("div");
    imageContainer.setAttribute("class", "imageContainer");

    //Adding a div for containing ratings and reviews
    let ratingReviewDiv = document.createElement("div");
    ratingReviewDiv.setAttribute("class", "ratingReviewDiv");
    ratingReviewDiv.style.display = "flex";

    //Adding an Image
    let imageDisplayer = document.createElement("img");
    imageDisplayer.setAttribute("src", eachData.image);

    //Adding the Title for each Product
    let headingForEachProduct = document.createElement("h2");
    headingForEachProduct.innerText = eachData.title;

    //Adding the category
    let categoryForEachProduct = document.createElement("p");
    categoryForEachProduct.innerText = "📌" + eachData.category;

    //Adding the Price
    let priceForEachProduct = document.createElement("h3");
    priceForEachProduct.innerHTML = "<span>$</span>" + eachData.price;

    //Adding the rating
    let ratingForEachProduct = document.createElement("h5");
    ratingForEachProduct.innerText = "⭐" + eachData.rating.rate;

    //Adding the totalReviews
    let reviewForEachProduct = document.createElement("h6");
    reviewForEachProduct.innerText = "👤" + eachData.rating.count;

    //Adding the "Add to Cart" Button
    let buttonToAddIntoCart = document.createElement("INPUT");
    buttonToAddIntoCart.setAttribute("type", "button");
    buttonToAddIntoCart.setAttribute("value", "Add to Cart");

    imageContainer.appendChild(imageDisplayer);
    ratingReviewDiv.appendChild(ratingForEachProduct);
    ratingReviewDiv.appendChild(reviewForEachProduct);

    creatingOneDiv.appendChild(categoryForEachProduct);
    creatingOneDiv.appendChild(imageContainer);
    creatingOneDiv.appendChild(ratingReviewDiv);
    creatingOneDiv.appendChild(headingForEachProduct);
    creatingOneDiv.appendChild(priceForEachProduct);
    creatingOneDiv.appendChild(buttonToAddIntoCart);

    return creatingOneDiv;
  });
  contentSection.append(...allData);
}

logoutButton.addEventListener("click", (event) => {
  event.preventDefault();
  console.log("Hello");
  localStorage.clear();
  setTimeout(() => {
    location.href = "./index.html";
  }, 2 * 1000);
});

signUp.addEventListener("click", (event) => {
  event.preventDefault();
  console.log("Hello");
  localStorage.clear();
  location.href = "./signUp.html";
});

allProducts();
checkUser();
