const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const mailAddress = document.querySelector("#mailAddress");
const passwordGiven = document.querySelector("#passwordGiven");
const confirmedPassword = document.querySelector("#repeatPasswordGiven");
const checkedBox = document.querySelector("#iAgree");
const submitButton = document.getElementById("myForm");

function signUpFunction() {
  let firstNameCondition = checkFirstName(firstName);
  let lastNameCondition = checkLastName(lastName);
  let mailCondition = checkMail(mailAddress);
  let passwordCondition = checkPassword(passwordGiven, confirmedPassword);
  let repeatPasswordCondition = checkRepeatPassword(confirmedPassword);
  let checkBoxConditionCheck = checkBoxCondition(checkedBox);
  let errorFinal = document.querySelector("#finalError");
  let successMessage = document.querySelector("#finalSuccess");

  console.log("getting checkbox", checkedBox);

  if (
    firstNameCondition &&
    lastNameCondition &&
    mailCondition &&
    passwordCondition &&
    repeatPasswordCondition &&
    checkBoxConditionCheck
  ) {
    successMessage.style.display = "block";
    errorFinal.style.display = "none";
    let fullName = `${firstName.value} ${lastName.value}`;
    let objectCreated = {
      name: fullName,
      mail: mailAddress.value,
    };
    localStorage.setItem("user", JSON.stringify(objectCreated));
    setTimeout(() => {
      location.href = "./index.html";
    }, 2 * 1000);
  } else {
    errorFinal.style.display = "block";
    successMessage.style.display = "none";
  }
}

function checkFirstName(name) {
  const regexForFirstName = /^[a-zA-Z]{2,30}$/;
  let errorName = document.querySelector("#firstNameError");

  if (regexForFirstName.test(name.value)) {
    errorName.style.display = "none";
    return true;
  } else {
    errorName.style.display = "block";
    return false;
  }
}

function checkLastName(name) {
  const regexForLastName = /^[a-zA-Z]{1,30}$/;
  let errorName = document.querySelector("#lastNameError");

  if (regexForLastName.test(name.value)) {
    errorName.style.display = "none";
    return true;
  } else {
    errorName.style.display = "block";
    return false;
  }
}

function checkMail(mailGiven) {
  const regexForEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  let errorName = document.querySelector("#mailError");
  console.log(mailGiven.value);
  if (regexForEmail.test(mailGiven.value)) {
    errorName.style.display = "none";
    return true;
  } else {
    errorName.style.display = "block";
    console.log("Hello Dis");
    return false;
  }
}

function checkPassword(passWord, passWordRepeat) {
  let errorPassword = document.querySelector("#passwordError");
  if (passWord.value.length > 6) {
    errorPassword.style.display = "none";
    console.log(passWordRepeat.value);
    console.log(passWord.value);
    let anotherError = document.querySelector("#repeatPasswordError");
    if (passWordRepeat.value === passWord.value) {
      anotherError.style.display = "none";
      return true;
    } else {
      anotherError.style.display = "block";
    }
  } else {
    errorPassword.style.display = "block";
    return false;
  }
}

function checkRepeatPassword(passWord) {
  let confirmedPasswordLeft = document.querySelector(
    "#repeatPasswordErrorBlank"
  );
  if (passWord.value.length === 0) {
    confirmedPasswordLeft.style.display = "block";
    return false;
  } else {
    confirmedPasswordLeft.style.display = "none";
    return true;
  }
}

function checkBoxCondition(checkBOX) {
  let errorCheck = document.querySelector("#checkedError");
  console.log(checkBOX.checked);
  if (checkBOX.checked === false) {
    errorCheck.style.display = "block";
    return false;
  } else {
    errorCheck.style.display = "none";
    return true;
  }
}

submitButton.addEventListener("submit", (event) => {
  event.preventDefault();
  console.log("Hello");
  signUpFunction();
});
